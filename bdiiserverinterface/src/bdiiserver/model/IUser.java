/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdiiserver.model;

/**
 *
 * @author b4p3p
 */
public interface IUser 
{
    String GetUsername();
    String GetPassword();
    int GetLevel();
}
