/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdiiserver.ejb.remote.stateful;

import bdiiserver.ejb.local.ContextManagerLocal;

/**
 *
 * @author b4p3p
 */
public interface LoginUserRemote {
    public boolean Login(String username , String password );
    public ContextManagerLocal GetContextManager();
}
