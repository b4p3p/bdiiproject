/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdii.db.model;

import bdiiserver.model.IUser;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author b4p3p
 */
public class User implements IUser {
    
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String LEVEL = "level";
    
    private String username ="";
    private String password ="";
    private int level = -1;
    
    public User (String _username , String _password , int _level)
    {
        username = _username;
        password = _password;
        level = _level;
    }

    public User(ResultSet executeQuery) throws SQLException {
        username = executeQuery.getString( User.USERNAME );
        password = executeQuery.getString( User.PASSWORD );
        level = executeQuery.getInt(User.LEVEL );
    }

    @Override
    public String GetUsername() {
        return username;
    }

    @Override
    public String GetPassword() {
        return password;
    }

    @Override
    public int GetLevel() {
        return level;
    }
    
}

enum LevelUsers 
{ 
    admin , // 0
    guest   // -1
}


