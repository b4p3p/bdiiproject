/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdii.db.controller;

import bdii.db.model.Table;
import bdii.db.model.User;
import bdii.log.LogManager;
import bdiiserver.ejb.local.ContextManagerLocal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContextManager {
    
    private final static String _USERNAME = "bdii";
    private final static String _PASSWORD = "bdii";
    
    private static Connection Connect()
    {
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = null;
            conn = DriverManager.getConnection( "jdbc:postgresql://localhost:5432/bdii",_USERNAME, _PASSWORD);
            return conn;
        } catch (ClassNotFoundException | SQLException ex) {
            LogManager.Error("ContextManager.Connect", ex);
            return null;
        }
    }
    
    private static void CloseConnection(Connection conn)
    {
        try {
            conn.close();
        } catch (SQLException ex) {
            LogManager.Error("CloseConnection", ex);
        }
    }
    
    private static void CloseConnection(PreparedStatement statement , Connection conn)
    {
        try {
            statement.close();
            conn.close();
        } catch (SQLException ex) {
            LogManager.Error("CloseConnection", ex);
        }
    }
    
    public static boolean IsUserExists(String user, String password)
    {
        try {
            String ris;
            
            Connection conn = Connect();
            
            String sql = String.format("SELECT EXISTS ( SELECT * FROM %s WHERE %s=? AND %s=? )" , Table.USERS , User.USERNAME , User.PASSWORD) ;
            
            PreparedStatement prepareStatement = conn.prepareStatement(sql);
            prepareStatement.setString(1, user);
            prepareStatement.setString(2, password);
            ResultSet executeQuery = prepareStatement.executeQuery();
            if ( executeQuery.next() )
            {
                ris = executeQuery.getString(1);
            }
            else
            {
                ris = "f";
            }
            
            CloseConnection(prepareStatement , conn);
            
            return ris.equals("t");
            
        } catch (SQLException ex) {
            LogManager.Error("IsUserExists", ex);
            return false;
        }
    }

    public static User GetUser(String _user, String _password) {
        try {
            
            Connection conn = Connect();
            
            String sql = String.format("SELECT * FROM %s WHERE %s=?" , Table.USERS , User.USERNAME) ;
            
            PreparedStatement prepareStatement = conn.prepareStatement(sql);
            prepareStatement.setString(1, _user);
            ResultSet executeQuery = prepareStatement.executeQuery();
            executeQuery.next();
            
            User user = new User ( executeQuery );
            
            CloseConnection(prepareStatement , conn);
               
            return user;
            
        } catch (SQLException ex) {
            LogManager.Error("GetError", ex);
            return null;
        }
        
    }
    
}
