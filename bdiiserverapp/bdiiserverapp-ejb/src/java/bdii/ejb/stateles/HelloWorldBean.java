package bdii.ejb.stateles;

import bdiiserver.ejb.remote.stateless.HelloWorldRemote;
import javax.ejb.Remote;
import javax.ejb.Stateless;


@Remote(HelloWorldRemote.class)
@Stateless ( name = "ejb/HelloWorldBean")
public class HelloWorldBean implements HelloWorldRemote{

    @Override
    public String SayHello() {
        return "Hello world";
    }

}
