/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdii.ejb.stateful;

import bdii.db.controller.ContextManager;
import bdii.db.model.User;
import bdii.log.LogManager;
import bdiiserver.ejb.local.ContextManagerLocal;

import bdiiserver.ejb.remote.stateful.LoginUserRemote;

import java.sql.SQLException;
import javax.ejb.Stateful;
import javax.ejb.Remote;

/**
 *
 * @author b4p3p
 */
@Remote(LoginUserRemote.class)
@Stateful ( name = "ejb/LoginUserBean")
public class LoginUserBean implements LoginUserRemote {

    private boolean flag_IsAuth = false;
    private User loggedUser = null;
    
    @Override
    public boolean Login(String _user, String _password) 
    {
        if ( ContextManager.IsUserExists(_user , _password ) )
        {
            LogManager.Info("connessione: " + _user );
            loggedUser = ContextManager.GetUser(_user , _password );
            flag_IsAuth = true;
            return false;
        }
        else
        {
            loggedUser = null;
            flag_IsAuth = true;
            return true;    
        }
        
    }

    @Override
    public ContextManagerLocal GetContextManager() {
        return null;
    }
    
    

}
