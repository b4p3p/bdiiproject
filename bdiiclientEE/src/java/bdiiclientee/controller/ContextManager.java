/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdiiclientee.controller;

import bdiiserver.ejb.remote.stateful.LoginUserRemote;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**

    public static LoginUserRemote GetLoginUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 *
 * @author b4p3p
 */
public class ContextManager 
{
    private static InitialContext context = null;
    private final static String _APPSERVER = "java:global/bdiiserverapp/bdiiserverapp-ejb/ejb/";
    private final static String _LOGINUSER = "LoginUserBean";
    
    //bdiiserver.ejb.remote.stateful.LoginUserRemote
    
    public static void Init() throws NamingException
    {
        context = new InitialContext();
    }
   
    public static LoginUserRemote GetLoginUser() {
        return (LoginUserRemote) GetBean( _APPSERVER + _LOGINUSER + "!" + LoginUserRemote.class.getName());
    }
    
    public static Object GetBean ( String name )
    {
        try {
            Object o = context.lookup(name);
            return o;
        } catch (NamingException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
}
