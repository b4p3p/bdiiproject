/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdiiclientee.view.controller;

import bdiiclientee.Main;
import bdiiclientee.log.LogManager;
import bdiiclientee.view.utils.RisLoadNode;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author b4p3p
 */
public class ViewManager 
{
    private static Stage primaryStage;
    
    public static void Init ( Stage stage)
    {
        primaryStage = stage;
    }
    
    private static Scene CreateScene(String url)
    {
        try {
            Parent root = FXMLLoader.load(Main.class.getResource(url));
            Scene scene = new Scene(root);
            return scene;
        } catch (IOException ex) {
            LogManager.Error("CreateScene", ex);
            return null;
        }
    }
    
    public static RisLoadNode LoadNode( String url)
    {
        try {
            
            FXMLLoader fxmlLoader = new FXMLLoader();
            Node node = (Node) fxmlLoader.load(Main.class.getResource(url).openStream());
            IController controller = (IController) fxmlLoader.getController();
            return new RisLoadNode( node , controller );
            
            //Parent node = FXMLLoader.load(Main.class.getResource(url));
            //return node;
            
        } catch (IOException ex) {
            LogManager.Error("CreateScene", ex);
            return null;
        }
    }
    
    public static void GoToLogin()
    {
        Scene scene = CreateScene("view/fxml/Login.fxml");
        if ( scene == null ) return;
        
        primaryStage.setTitle("bdii project login");
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }
    
    public static void GoToMain()
    {
        Scene scene = CreateScene("view/fxml/Main.fxml");
        if ( scene == null ) return;
        
        ClearMainContainer(scene);
        
        primaryStage.setTitle("bdii project");
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }
    
    public static void ClearMainContainer (Scene scene)
    {
        VBox container =  (VBox) scene.lookup("#container");
        container.getChildren().clear();
    }
    
}
