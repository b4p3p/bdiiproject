package bdiiclientee.view.controller;

import bdiiclientee.Main;
import bdiiclientee.controller.ContextManager;
import bdiiserver.ejb.remote.stateful.LoginUserRemote;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.naming.NamingException;


/**
 * Created by b4p3p on 30/01/14.
 */
public class LoginController {

    @FXML private TextField txtUsername;
    @FXML private PasswordField txtPassword;
    @FXML private Label lblAuthError;

    public LoginController()
    {
        
    }

    public void cmdLoginClick(ActionEvent actionEvent)  {

        Main.loginUserRemote = ContextManager.GetLoginUser();
        boolean test = Main.loginUserRemote.Login(txtUsername.getText(), txtPassword.getText());
        if ( test == false )
        {
            ViewManager.GoToMain();
            lblAuthError.setVisible(false);
        }
        else
        {
            lblAuthError.setVisible(true);
        }
        txtPassword.setText("");

    }

}
