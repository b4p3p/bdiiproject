/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdiiclientee.view.controller;

import bdiiclientee.view.controller.main.UsersController;
import bdiiclientee.view.utils.RisLoadNode;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author b4p3p
 */
public class MainController 
{

    @FXML HBox hboxMenuBar;
    @FXML VBox container;
    
    private void DeselezionaTutto() {
        
        ObservableList<Node> children = hboxMenuBar.getChildren();
        for ( Node n : children)
        {
            StackPane s = (StackPane)n;
            s.getStyleClass().clear();
            s.getStyleClass().add("itemNonSelezionato");
        }
    }
    
    private void SelezionaItem(String id) {
        
         DeselezionaTutto();
         
         StackPane rec = (StackPane) hboxMenuBar.lookup(id);
         rec.getStyleClass().clear();
         rec.getStyleClass().add("itemSelezionato");
    }
    
    private void ClearContent() {
        container.getChildren().clear();
    }
    
    private void AddContent( Node nodo ) {
        container.getChildren().add(nodo);
    }
    
    /* BUTTON CLICK */
    
    public void cmdUserClick(ActionEvent actionEvent)  {
        
        ClearContent();
        SelezionaItem("#stkCmdUsers");
        
        RisLoadNode rec = ViewManager.LoadNode("view/fxml/main/frmUsers.fxml");
        
        AddContent(rec.nodo);
        
        UsersController ctrl = (UsersController) rec.controller;
        ctrl.LoadUsers();
       
    }
    
    public void cmdDBClick(ActionEvent actionEvent)  {
 
        ClearContent();
        SelezionaItem("#stkCmdDB");
        
      
    }
    
    /* -END- BUTTON CLICK */     
    
}
