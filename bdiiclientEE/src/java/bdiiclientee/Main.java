/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bdiiclientee;

import bdiiclientee.controller.ContextManager;
import bdiiclientee.log.LogManager;
import bdiiclientee.view.controller.ViewManager;
import bdiiserver.ejb.remote.stateful.LoginUserRemote;
import java.io.IOException;
import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.naming.NamingException;

/**
 *
 * @author b4p3p
 */

public class Main extends Application {
    
    public static LoginUserRemote loginUserRemote;
    public static Stage primaryStage;
    
    @Override
    public void start(Stage primaryStage) throws IOException, Exception 
    {
        try {
            ContextManager.Init();
        } catch (NamingException ex) 
        {
            LogManager.Error("start", ex);
            return;
        }
        
        ViewManager.Init(primaryStage);
        ViewManager.GoToLogin();
        
    }
    
    public static void main(String[] args) {
        launch(args);
    }
   
    
}




/*
public class Main {

    public static void main(String[] args) throws NamingException {
        
        //GetBean(context, "java:global/bdiiserverapp/HelloWorldBean!bdii.remote.HelloWorldRemote");
        //HelloWorld hello = (HelloWorld) GetBean(context, "java:global/bdiiserverapp/bdiiserverapp-ejb/ejb/HelloWorld!bdiiserver.ejb.remote.stateless.HelloWorld");
        //LoginUser login = (LoginUser) GetBean(context, "java:global/bdiiserverapp/bdiiserverapp-ejb/ejb/LoginUser!bdiiserver.ejb.remote.stateful.LoginUser");
        //System.out.println("operazione: " + login.Login("asd", "asd"));
        
        System.out.println("fatto!");
        
    }
    
}

*/
